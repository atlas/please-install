#!/bin/bash

source ../common.sh

LIB_NAME="petsc"
LIB_VERSION=3.16.3 #3.15.1 #3.12.1 # 3.9.3 #3.9.1 #3.8.3 #3.7.6
GCC_VERSION=8.3.0 #6.4.0
MPI_LIB=openmpi
MPI_VERSION=4.0.2 #1.10.7 #2.1.1 #1.10.7
HDF5_VERSION=1.10.5

LIB_FULLNAME=${LIB_NAME}-${LIB_VERSION}
GCC_FULL=gcc-$GCC_VERSION
GCC_SHORT="gcc${GCC_VERSION//.}"
MPI_FULL=${MPI_LIB}-${MPI_VERSION}
MPI_SHORT="${MPI_LIB}${MPI_VERSION//.}"
SUB_DIR=${LIB_NAME}/${LIB_VERSION}/${GCC_FULL}/${MPI_FULL}
WORK_DIR=${BASE_WORK_DIR}/${SUB_DIR}
SRC_DIR=${WORK_DIR}/${LIB_FULLNAME}
ARCHIVE=${SRC_DIR}.tar.gz
URL="http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-lite-${LIB_VERSION}.tar.gz"
BUILD_DIR=${WORK_DIR}/${LIB_FULLNAME}-build
INSTALL_DIR=${BASE_INSTALL_DIR}/${SUB_DIR}
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
MODULE_DIR=${BASE_MODULE_DIR}/libs/${LIB_NAME}
MODULE_PATH=${MODULE_DIR}/${LIB_VERSION}_${GCC_SHORT}_${MPI_SHORT}

PETSC_LOAD_MODULES="module load intel-mkl/2019.5.281"
install_lib()
{
module purge
module load gcc/${GCC_VERSION}
module load ${MPI_LIB}/${MPI_VERSION}_${GCC_SHORT}
module load hdf5/${HDF5_VERSION}_${GCC_SHORT}_${MPI_SHORT}
module load intel-mkl/2019.5.281

module load mpi4py/3.1.3_${GCC_SHORT}_${MPI_SHORT}_py3 
#WARNING : load paraview only for mpi4py (TODO : externalize this call)
#module load paraview/5.7.0-py3_${GCC_SHORT}_${MPI_SHORT}
#MPI4PY_DIR=/data/software/install/paraview/5.7.0-py3/gcc-8.3.0/openmpi-4.0.2/lib/python3.5/site-packages/mpi4py
module list

gcc --version |head -n 1
sleep 2
mpirun --version |head -n 1
sleep 2

if [[ ! -f $ARCHIVE ]]; then
  mkdir -p $WORK_DIR
  wget $URL -O $ARCHIVE
fi

if [[ ! -d $BUILD_DIR ]]; then
  mkdir $BUILD_DIR
  tar zxf $ARCHIVE --directory $BUILD_DIR
fi

cd $BUILD_DIR/petsc-${LIB_VERSION} || exit 1

if [ ] ; then
./configure --with-shared-libraries=1 \
            --prefix=${INSTALL_DIR} \
            --with-cc=`which mpicc` \
            --with-cxx=`which mpic++` \
            --with-fc=`which mpif90` \
            --with-mpiexec=`which mpiexec` \
            --download-suitesparse=1  \
            --download-ml  \
            --download-blacs  \
            --download-scalapack \
            --download-fblaslapack  \
            --download-mumps  \
            --download-hypre  \
            --download-ptscotch \
            --with-hdf5=1 \
            --with-hdf5-dir=$HDF5_ROOT \
	    --download-petsc4py=1  --with-mpi4py=1 \
            --with-debugging=0 \
            --COPTFLAGS='-O3'  --CXXOPTFLAGS='-O3'  --FOPTFLAGS='-O3' || exit 1
else

    #--with-blaslapack-dir=${MKL_ROOT} --with-scalapack-lib="-L${MKL_ROOT}/lib/intel64 -lmkl_scalapack_lp64 -lmkl_blacs_openmpi_lp64" --with-scalapack-include=${MKL_ROOT}/include --with-mkl_cpardiso-dir=${MKL_ROOT} \
    # --with-openmp --with-openmp-include=${INTEL_ROOT}/lib/intel64 --with-openmp-lib=${INTEL_ROOT}/lib/intel64/libiomp5.so \
    #  --with-mkl_cpardiso-dir=${MKL_ROOT} \
    # 	    --with-64-bit-blas-indices=1 --known-64-bit-blas-indices=1 \
    #--with-blaslapack-include=${MKL_ROOT}/include \
    #--with-blaslapack-lib="-L${MKL_ROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_scalapack_ilp64 -lmkl_intel_ilp64 -lmkl_intel_thread -lmkl_core -lmkl_blacs_openmpi_ilp64  -L${INTEL_ROOT}/lib/intel64 -liomp5 -lpthread -lm -ldl" \
    # --with-mkl_cpardiso=1 \

    # with MKL
./configure --with-shared-libraries=1 \
            --prefix=${INSTALL_DIR} \
            --with-cc=`which mpicc` \
            --with-cxx=`which mpic++` \
            --with-fc=`which mpif90` \
            --with-mpiexec=`which mpiexec` \
            --download-suitesparse=1  \
            --download-ml  \
	    --with-blaslapack-include=${MKL_ROOT}/include \
	    --with-blaslapack-lib="-L${MKL_ROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_scalapack_lp64 -lmkl_cdft_core -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -lmkl_blacs_openmpi_lp64  -L${INTEL_ROOT}/lib/intel64 -liomp5 -lpthread -lm -ldl" \
	    --with-scalapack-lib="-L${MKL_ROOT}/lib/intel64 -lmkl_scalapack_lp64 -lmkl_blacs_openmpi_lp64" --with-scalapack-include=${MKL_ROOT}/include \
	    --with-mkl_pardiso-dir=${MKL_ROOT} --with-mkl_cpardiso-dir=${MKL_ROOT} \
            --download-mumps  \
            --download-hypre  \
            --download-ptscotch \
            --with-hdf5=1 \
            --with-hdf5-dir=$HDF5_ROOT \
	    --with-petsc4py=1 --with-petsc4py-test-np=6 \
            --with-debugging=0 \
            --COPTFLAGS='-O3'  --CXXOPTFLAGS='-O3'  --FOPTFLAGS='-O3' || exit 1

#--with-petsc4py=1 --with-petsc4py-test-np=6  --with-mpi4py=1 --with-mpi4py-dir=$MPI4PY_DIR \
#  -DMKL_ILP64 -m64
fi

#make PETSC_DIR=`pwd` PETSC_ARCH="arch-linux2-c-opt" all || exit 1
#make PETSC_DIR=`pwd` PETSC_ARCH="arch-linux2-c-opt" install || exit 1
make PETSC_DIR=`pwd` PETSC_ARCH="arch-linux-c-opt" all || exit 1
make PETSC_DIR=`pwd` PETSC_ARCH="arch-linux-c-opt" install || exit 1
}

install_module()
{
cd $SCRIPT_DIR
mkdir -p ${MODULE_DIR}

export LIB_NAME
export LIB_VERSION
export INSTALL_DIR
export PETSC_LOAD_MODULES
envtpl  --keep-template -o $MODULE_PATH module.tmpl
}

if [[ $1 == "module" ]]
then
  install_module
elif [[ $1 == "clean" ]]
then
  clean_all
else
  install_lib
  install_module
fi

