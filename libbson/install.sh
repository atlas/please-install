#!/bin/bash

source ../common.sh

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

LIB_NAME="libbson"
LIB_VERSION=1.9.5
LIB_FULLNAME=${LIB_NAME}-${LIB_VERSION}
GCC_VERSION=8.3.0 #6.4.0
GCC_FULL=gcc-$GCC_VERSION
GCC_SHORT="gcc${GCC_VERSION//.}"

URL=https://github.com/mongodb/libbson/releases/download/${LIB_VERSION}/${LIB_FULLNAME}.tar.gz

SUB_DIR=${LIB_NAME}/${LIB_VERSION}/${GCC_FULL}
INSTALL_DIR=${BASE_INSTALL_DIR}/${SUB_DIR}
WORK_DIR=${BASE_WORK_DIR}/${SUB_DIR}
BUILD_DIR=${WORK_DIR}/${LIB_FULLNAME}-build
ARCHIVE=${WORK_DIR}/${LIB_FULLNAME}.tar.gz
SRC_DIR=${WORK_DIR}/${LIB_FULLNAME}

MODULE_DIR=${BASE_MODULE_DIR}/libs/${LIB_NAME}
MODULE_PATH=${MODULE_DIR}/${LIB_VERSION}_${GCC_SHORT}

install_lib()
{
    module purge
    module load gcc/${GCC_VERSION}
    module list

    # Check gcc version here
    gcc --version |head -n 1
    sleep 2

    if [[ ! -f $ARCHIVE ]]; then
	mkdir -p $WORK_DIR
	wget $URL -O $ARCHIVE
    fi

    if [[ ! -d $SRC_DIR ]]; then
	tar zxf $ARCHIVE --directory $WORK_DIR
    fi

    if [[ ! -d $BUILD_DIR ]]; then
	mkdir $BUILD_DIR
	cd $BUILD_DIR
    fi

    ${SRC_DIR}/configure --prefix=${INSTALL_DIR}
    make -j
    make install
}

install_module()
{
    cd $SCRIPT_DIR
    mkdir -p ${MODULE_DIR}

    export LIB_NAME
    export LIB_VERSION
    export INSTALL_DIR
    envtpl  --keep-template -o $MODULE_PATH module.tmpl
}

if [[ $1 == "module" ]]
then
    install_module
elif [[ $1 == "clean" ]]
then
  clean_all
else
    install_lib
    install_module
fi
