#!/bin/bash

source ../common.sh

LIB_NAME="med"
LIB_VERSION=4.0.0
GCC_VERSION=8.3.0 #6.4.0
MPI_LIB=openmpi
MPI_VERSION=4.0.2 #1.10.7 #2.1.1 #1.10.7
MPI_SHORT="${MPI_LIB}${MPI_VERSION//.}"
MPI_FULL=${MPI_LIB}-${MPI_VERSION}

LIB_FULLNAME=${LIB_NAME}-${LIB_VERSION}
GCC_FULL=gcc-$GCC_VERSION
GCC_SHORT="gcc${GCC_VERSION//.}"
SUB_DIR=${LIB_NAME}/${LIB_VERSION}/${GCC_FULL}/${MPI_FULL}
WORK_DIR=${BASE_WORK_DIR}/${SUB_DIR}
SRC_DIR=${WORK_DIR}/${LIB_FULLNAME}
ARCHIVE=${WORK_DIR}/${LIB_FULLNAME}.tar.gz
URL="http://files.salome-platform.org/Salome/other/med-${LIB_VERSION}.tar.gz"

BUILD_DIR=${WORK_DIR}/${LIB_FULLNAME}-build
INSTALL_DIR=${BASE_INSTALL_DIR}/${SUB_DIR}
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
MODULE_DIR=${BASE_MODULE_DIR}/libs/${LIB_NAME}
MODULE_PATH=${MODULE_DIR}/${LIB_VERSION}_${GCC_SHORT}_${MPI_SHORT}

install_lib()
{
module purge
module load gcc/${GCC_VERSION}
module load ${MPI_LIB}/${MPI_VERSION}_${GCC_SHORT}
module load hdf5/1.10.5_${GCC_SHORT}_${MPI_SHORT}
module list

gcc --version |head -n 1
sleep 2
mpirun --version |head -n 1
sleep 2

if [[ ! -f $ARCHIVE ]]; then
  mkdir -p $WORK_DIR
  wget $URL -O $ARCHIVE
fi

if [[ ! -d $SRC_DIR ]]; then
  tar zxf $ARCHIVE --directory $WORK_DIR
fi

if [[ ! -d $BUILD_DIR ]]; then
  mkdir $BUILD_DIR
  cd $BUILD_DIR

  cmake -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} \
	-DCMAKE_BUILD_TYPE=release \
	-DMEDFILE_USE_MPI=ON \
	-DMEDFILE_BUILD_TESTS=OFF \
	-DHDF5_ROOT_DIR=${HDF5_ROOT} \
	${SRC_DIR}
fi

cd $BUILD_DIR
make -j || exit 1
make -j install || exit 1
}

install_module()
{
cd $SCRIPT_DIR
mkdir -p ${MODULE_DIR}

export LIB_NAME
export LIB_VERSION
export INSTALL_DIR
envtpl  --keep-template -o $MODULE_PATH module.tmpl
}

if [[ $1 == "module" ]]
then
  install_module
elif [[ $1 == "clean" ]]
then
  clean_all
else
  install_lib
  install_module
fi

