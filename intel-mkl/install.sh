#!/bin/bash

source ../common.sh

LIB_NAME="intel-mkl"
LIB_VERSION=2019.5.281

LIB_FULLNAME=${LIB_NAME}-${LIB_VERSION}
SUB_DIR=${LIB_NAME}/${LIB_VERSION}
WORK_DIR=${BASE_WORK_DIR}/${SUB_DIR}

SRC_DIR=${WORK_DIR}/l_mkl_${LIB_VERSION}
ARCHIVE=${SRC_DIR}.tgz
INSTALL_DIR=${BASE_INSTALL_DIR}/${SUB_DIR}
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
MODULE_DIR=${BASE_MODULE_DIR}/libs/${LIB_NAME}
MODULE_PATH=${MODULE_DIR}/${LIB_VERSION}

install_lib()
{
module purge

if [[ ! -f $ARCHIVE ]]; then
  mkdir -p $WORK_DIR
  #wget $URL -O $ARCHIVE
  #TODO mettre l'archive quelque parts
  exit 1
fi

if [[ ! -d $SRC_DIR ]]; then
    tar zxf $ARCHIVE --directory $WORK_DIR || exit 1
fi

if [[ ! -f $WORK_DIR/my_silent_config.cfg  ]]; then
    export INSTALL_DIR
    envtpl  --keep-template -o $WORK_DIR/my_silent_config.cfg my_silent_config.cfg.tmpl || exit 1
fi

if [[ ! -d $INSTALL_DIR ]]; then
    $SRC_DIR/install.sh --silent "$WORK_DIR/my_silent_config.cfg"
fi

}

install_module()
{
cd $SCRIPT_DIR
mkdir -p ${MODULE_DIR}

export LIB_NAME
export LIB_VERSION
export INSTALL_DIR
envtpl  --keep-template -o $MODULE_PATH module.tmpl
}

if [[ $1 == "module" ]]
then
  install_module
elif [[ $1 == "clean" ]]
then
  clean_all
else
  install_lib
  install_module
fi

