#!/bin/bash

source ../common.sh

LIB_VERSION=develop #feature/fix_machine_atlas  #develop #feature/toolboxes-refactoring #develop
#LIB_VERSION=v0.108.0 #v0.107.0 # v0.106.0 #v0.105.0 #v0.104.0
GCC_VERSION=8.3.0 #6.4.0
MPI_LIB=openmpi
MPI_VERSION=4.0.2 #1.10.7 #2.1.1 #1.10.7
GCC_FULL=gcc-$GCC_VERSION
GCC_SHORT="gcc${GCC_VERSION//.}"
MPI_FULL=${MPI_LIB}-${MPI_VERSION}
MPI_SHORT="${MPI_LIB}${MPI_VERSION//.}"

LIB_NAME="feelpp-lib"
LIB_QUICKSTART_NAME="feelpp-quickstart"
LIB_TOOLBOXES_NAME="feelpp-toolboxes"
LIB_FULLNAME=${LIB_NAME}-${LIB_VERSION}
LIB_QUICKSTART_FULLNAME=${LIB_QUICKSTART_NAME}-${LIB_VERSION}
LIB_TOOLBOXES_FULLNAME=${LIB_TOOLBOXES_NAME}-${LIB_VERSION}
SUB_LIB_DIR=${LIB_NAME}/${LIB_VERSION}/${GCC_FULL}/${MPI_FULL}
SUB_QUICKSTART_DIR=${LIB_QUICKSTART_NAME}/${LIB_VERSION}/${GCC_FULL}/${MPI_FULL}
SUB_TOOLBOXES_DIR=${LIB_TOOLBOXES_NAME}/${LIB_VERSION}/${GCC_FULL}/${MPI_FULL}
SRC_DIR=${BASE_WORK_DIR}/${SUB_LIB_DIR}
GIT_URL="https://github.com/feelpp/feelpp.git"
BUILD_LIB_DIR=${SRC_DIR}/build-lib
BUILD_QUICKSTART_DIR=${SRC_DIR}/build-quickstart
BUILD_TOOLBOXES_DIR=${SRC_DIR}/build-toolboxes
INSTALL_LIB_DIR=${BASE_INSTALL_DIR}/${SUB_LIB_DIR}
INSTALL_QUICKSTART_DIR=${BASE_INSTALL_DIR}/${SUB_QUICKSTART_DIR}
INSTALL_TOOLBOXES_DIR=${BASE_INSTALL_DIR}/${SUB_TOOLBOXES_DIR}
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
MODULE_LIB_DIR=${BASE_MODULE_DIR}/libs/${LIB_NAME}
MODULE_LIB_PATH=${MODULE_LIB_DIR}/${LIB_VERSION}_${GCC_SHORT}_${MPI_SHORT}
MODULE_LIB_NAME=${LIB_NAME}/${LIB_VERSION}_${GCC_SHORT}_${MPI_SHORT}
MODULE_QUICKSTART_DIR=${BASE_MODULE_DIR}/libs/${LIB_QUICKSTART_NAME}
MODULE_QUICKSTART_PATH=${MODULE_QUICKSTART_DIR}/${LIB_VERSION}_${GCC_SHORT}_${MPI_SHORT}
MODULE_TOOLBOXES_DIR=${BASE_MODULE_DIR}/libs/${LIB_TOOLBOXES_NAME}
MODULE_TOOLBOXES_PATH=${MODULE_TOOLBOXES_DIR}/${LIB_VERSION}_${GCC_SHORT}_${MPI_SHORT}

FEELPP_PROFILE=feelpp.profile_${GCC_SHORT}_${MPI_SHORT}

install_lib()
{
module purge
module load ${FEELPP_PROFILE}
module list

gcc --version |head -n 1
sleep 2
mpirun --version |head -n 1
sleep 2

if [[ ! -d $SRC_DIR/feelpp ]]; then
  mkdir -p $SRC_DIR
  cd  $SRC_DIR
  git clone $GIT_URL
  cd  $SRC_DIR/feelpp
  git checkout $LIB_VERSION
else
  cd  $SRC_DIR/feelpp && git checkout develop && git pull && git checkout $LIB_VERSION
fi
git submodule update --init --recursive
    
rm -rf $BUILD_LIB_DIR
mkdir $BUILD_LIB_DIR
cd $BUILD_LIB_DIR || exit 1
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_CXX_COMPILER=clang++ \
      -DCMAKE_C_COMPILER=clang \
      -DCMAKE_INSTALL_PREFIX=${INSTALL_LIB_DIR} \
      -DFEELPP_ENABLE_QUICKSTART=OFF \
      -DFEELPP_ENABLE_DOCUMENTATION=OFF -DFEELPP_ENABLE_BENCHMARKS=OFF -DFEELPP_ENABLE_TESTS=OFF \
      -DFEELPP_PARMMG_DOWNLOAD_METIS=ON \
      $SRC_DIR/feelpp || exit 1
#      -DFEELPP_ENABLE_OMC=OFF  -DFEELPP_ENABLE_FMILIB=OFF \
cd $BUILD_LIB_DIR/data && make install || exit 1
cd $BUILD_LIB_DIR/feelpp && make install -j16 || exit 1
}

install_lib_quickstart()
{
    module purge
    module load feelpp-lib/${LIB_VERSION}_${GCC_SHORT}_${MPI_SHORT}
    module list

    if [[ ! -d $SRC_DIR/feelpp ]]; then
	echo "no src dir : exit"
	exit 1
    fi

    echo "start configure of quickstart with FEELPP_DIR=${FEELPP_DIR}" 
    
    rm -rf $BUILD_QUICKSTART_DIR
    mkdir $BUILD_QUICKSTART_DIR
    cd $BUILD_QUICKSTART_DIR || exit 1

    cmake -DCMAKE_BUILD_TYPE=Release \
	  -DCMAKE_CXX_COMPILER=clang++ \
	  -DCMAKE_C_COMPILER=clang \
	  -DCMAKE_INSTALL_PREFIX=${INSTALL_QUICKSTART_DIR} \
	  -DFEELPP_DIR=${FEELPP_DIR} \
	  $SRC_DIR/feelpp/feelpp/quickstart || exit 1
    make install -j16
}

install_lib_toolboxes()
{
    module purge
    module load feelpp-lib/${LIB_VERSION}_${GCC_SHORT}_${MPI_SHORT}
    module list

    if [[ ! -d $SRC_DIR/feelpp ]]; then
	echo "no src dir : exit"
	exit 1
    fi

    echo "start configure of toolboxes with FEELPP_DIR=${FEELPP_DIR}" 
    
    rm -rf $BUILD_TOOLBOXES_DIR
    mkdir $BUILD_TOOLBOXES_DIR
    cd $BUILD_TOOLBOXES_DIR || exit 1

    cmake -DCMAKE_BUILD_TYPE=Release \
	  -DCMAKE_CXX_COMPILER=clang++ \
	  -DCMAKE_C_COMPILER=clang \
	  -DCMAKE_INSTALL_PREFIX=${INSTALL_TOOLBOXES_DIR} \
	  -DFEELPP_DIR=${FEELPP_DIR} \
	  $SRC_DIR/feelpp/toolboxes || exit 1
    make install -j16
}

install_module_lib()
{
    cd $SCRIPT_DIR
    MODULE_LIB_DIR_REAL="$(dirname ${MODULE_LIB_PATH})"
    #mkdir -p ${MODULE_LIB_DIR}
    mkdir -p ${MODULE_LIB_DIR_REAL}

    export LIB_NAME
    export LIB_VERSION
    export INSTALL_LIB_DIR
    export FEELPP_PROFILE

    module purge
    module load ${FEELPP_PROFILE}
    export FEELPP_LOAD_MODULES=$(module list 2>&1  |  perl -pe 's/ *\)*  /\n/g' | sed '/^\s*$/d' | sed '/[0-9][)][ ]/!d' | sort -n | sed 's/[0-9]*[)][ ]//g' | sed 's/^[ \t]*//' | sed "s/$(echo ${FEELPP_PROFILE})//g" | sed '/^\s*$/d' | sed 's/^/module load /')

    envtpl  --keep-template -o $MODULE_LIB_PATH module_lib.tmpl

}

install_module_quickstart()
{
    cd $SCRIPT_DIR
    MODULE_QUICKSTART_DIR_REAL="$(dirname ${MODULE_QUICKSTART_PATH})"
    #mkdir -p ${MODULE_QUICKSTART_DIR}
    mkdir -p ${MODULE_QUICKSTART_DIR_REAL}

    export LIB_QUICKSTART_NAME
    export LIB_VERSION
    export INSTALL_QUICKSTART_DIR
    export MODULE_LIB_NAME
    envtpl  --keep-template -o $MODULE_QUICKSTART_PATH module_quickstart.tmpl
}

install_module_toolboxes()
{
    cd $SCRIPT_DIR
    MODULE_TOOLBOXES_DIR_REAL="$(dirname ${MODULE_TOOLBOXES_PATH})"
    #mkdir -p ${MODULE_TOOLBOXES_DIR}
    mkdir -p ${MODULE_TOOLBOXES_DIR_REAL}

    export LIB_TOOLBOXES_NAME
    export LIB_VERSION
    export INSTALL_TOOLBOXES_DIR
    export MODULE_LIB_NAME
    envtpl  --keep-template -o $MODULE_TOOLBOXES_PATH module_toolboxes.tmpl
}

if [[ $1 == "module" ]]; then
    install_module_lib
    install_module_quickstart
    install_module_toolboxes
    exit 1
fi

if [[ $1 == "feelpp-lib" ]]; then
    install_lib
    install_module_lib
    exit 1
fi

if [[ $1 == "feelpp-quickstart" ]]; then
    install_lib_quickstart
    install_module_quickstart
    exit 1
fi

if [[ $1 == "feelpp-toolboxes" ]]; then
    install_lib_toolboxes
    install_module_toolboxes
    exit 1
fi

if [[ $1 == "remove" ]]; then
    rm -rI $BUILD_LIB_DIR
    rm -rI $BUILD_QUICKSTART_DIR
    rm -rI $BUILD_TOOLBOXES_DIR
    rm -rI $INSTALL_LIB_DIR
    rm -rI $INSTALL_QUICKSTART_DIR
    rm -rI $INSTALL_TOOLBOXES_DIR
    rm -I $MODULE_LIB_PATH
    rm -I $MODULE_QUICKSTART_PATH
    rm -I $MODULE_TOOLBOXES_PATH
    exit 1
fi
install_lib
install_module_lib
install_lib_quickstart
install_module_quickstart
install_lib_toolboxes
install_module_toolboxes
