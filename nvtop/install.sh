#!/bin/bash

source ../common.sh

LIB_NAME="nvtop"
URL="https://github.com/Syllo/nvtop.git"

install_lib()
{
module load cmake

cd $BASE_WORK_DIR
git clone $URL
cd $LIB_NAME

mkdir -p build && cd build
#cmake ..

# If it errors with "Could NOT find NVML (missing: NVML_INCLUDE_DIRS)"
# try the following command instead, otherwise skip to the build with make.
cmake .. -DNVML_RETRIEVE_HEADER_ONLINE=True

make -j
make install # You may need sufficent permission for that (root)
}

install_lib
