#!/bin/bash
#set -x

source ../common.sh

LIB_NAME="fmilib"
LIB_VERSION=2.0.3
GCC_VERSION=8.3.0
GCC_FULL=gcc-$GCC_VERSION
GCC_SHORT="gcc${GCC_VERSION//.}"

SUB_DIR=${LIB_NAME}/${LIB_VERSION}/${GCC_FULL}
WORK_DIR=${BASE_WORK_DIR}/${SUB_DIR}

SRC_DIR=${WORK_DIR}/fmi-library-${LIB_VERSION}
BUILD_DIR=${WORK_DIR}/${LIB_FULLNAME}-build
INSTALL_DIR=${BASE_INSTALL_DIR}/${SUB_DIR}
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Use cern repository! (patched version)
URL="https://github.com/modelon-community/fmi-library/archive/${LIB_VERSION}.tar.gz"

MODULE_CATEGORY="libs"
MODULE_DIR=${BASE_MODULE_DIR}/${MODULE_CATEGORY}/${LIB_NAME}
MODULE_PATH=${MODULE_DIR}/${LIB_VERSION}_${GCC_SHORT}

ARCHIVE=${WORK_DIR}/${LIB_VERSION}.zip

install_lib()
{
    module purge
    module load gcc/${GCC_VERSION}
    module load cmake/3.9.2_gcc640
    module list

    # Check gcc version here
    gcc --version |head -n 1
    sleep 2

    if [[ ! -f $ARCHIVE ]]; then
	mkdir -p $WORK_DIR
	wget $URL -O $ARCHIVE
    fi

    if [[ ! -d $SRC_DIR ]]; then
	 tar zxf $ARCHIVE --directory $WORK_DIR
    fi

    if [[ ! -d $BUILD_DIR ]]; then
	mkdir $BUILD_DIR
	cd $BUILD_DIR

	cmake -DCMAKE_C_COMPILER=`which gcc` \
              -DCMAKE_CXX_COMPILER=`which g++` \
              -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}/ \
              -DFMILIB_INSTALL_PREFIX=${INSTALL_DIR}/ \
              -DCMAKE_BUILD_TYPE=Release \
	      ${SRC_DIR}
    fi

    cd $BUILD_DIR
    make -j || exit 1
    make -j install || exit 1

}

install_module()
{
    cd $SCRIPT_DIR
    mkdir -p ${MODULE_DIR}

    export LIB_NAME
    export LIB_VERSION
    export INSTALL_DIR
    export MODULE_CATEGORY
    envtpl  --keep-template -o $MODULE_PATH module.tmpl
}

if [[ $1 == "module" ]]
then
    install_module
else
    install_lib
    install_module
fi
