#!/bin/bash

set -x

# You may tune this section
SOFT_DIR="/data/software"
BASE_WORK_DIR=${SOFT_DIR}/sources
BASE_INSTALL_DIR=${SOFT_DIR}/install
BASE_MODULE_DIR=${SOFT_DIR}/modules
###

mkdir -p $BASE_WORK_DIR
mkdir -p $BASE_INSTALL_DIR
mkdir -p $BASE_MODULE_DIR

download()
{
if [[ ! -f $ARCHIVE ]]; then
  mkdir -p $WORK_DIR
  wget $URL -O $ARCHIVE || { echo 'Download failed' ; exit 1; }
fi
}

clean_dir()
{
if [[ -d $1 ]]
then
  rm -rf $1
else
  echo "WARNING: $1 does not exist"
fi
}

clean_all()
{
  clean_dir $SRC_DIR
  clean_dir $BUILD_DIR
}
