#!/bin/bash

source ../common.sh

LIB_NAME="mpi4py"
LIB_VERSION=3.1.3
PYTHON_MAJOR_VERSION=3
GCC_VERSION=8.3.0 #6.4.0
MPI_LIB=openmpi
MPI_VERSION=4.0.2 # 1.10.7 #2.1.1 #1.10.7
MPI_SHORT="${MPI_LIB}${MPI_VERSION//.}"
MPI_FULL=${MPI_LIB}-${MPI_VERSION}

LIB_FULLNAME=${LIB_NAME}-${LIB_VERSION}
GCC_FULL=gcc-$GCC_VERSION
GCC_SHORT="gcc${GCC_VERSION//.}"
SUB_DIR=${LIB_NAME}/${LIB_VERSION}/${GCC_FULL}/${MPI_FULL}/python${PYTHON_MAJOR_VERSION}
WORK_DIR=${BASE_WORK_DIR}/${SUB_DIR}
SRC_DIR=${WORK_DIR}/${LIB_FULLNAME}
ARCHIVE=${WORK_DIR}/${LIB_FULLNAME}.tgz
URL="https://github.com/mpi4py/mpi4py/releases/download/${LIB_VERSION}/mpi4py-${LIB_VERSION}.tar.gz"

BUILD_DIR=${WORK_DIR}/${LIB_FULLNAME}-build
INSTALL_DIR=${BASE_INSTALL_DIR}/${SUB_DIR}
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
MODULE_DIR=${BASE_MODULE_DIR}/libs/${LIB_NAME}
MODULE_PATH=${MODULE_DIR}/${LIB_VERSION}_${GCC_SHORT}_${MPI_SHORT}_py${PYTHON_MAJOR_VERSION}

install_lib()
{
module purge
module load gcc/${GCC_VERSION}
module load ${MPI_LIB}/${MPI_VERSION}_${GCC_SHORT}
module list

mpicc --version | head -n 1
python${PYTHON_MAJOR_VERSION} --version
sleep 2

if [[ ! -f $ARCHIVE ]]; then
  mkdir -p $WORK_DIR
  wget $URL -O $ARCHIVE
fi

if [[ ! -d $SRC_DIR ]]; then
  tar zxf $ARCHIVE --directory $WORK_DIR
fi

cd $SRC_DIR
python${PYTHON_MAJOR_VERSION} ${SRC_DIR}/setup.py build
python${PYTHON_MAJOR_VERSION} ${SRC_DIR}/setup.py install --prefix ${INSTALL_DIR}

}

install_module()
{
cd $SCRIPT_DIR
mkdir -p ${MODULE_DIR}

export LIB_NAME
export LIB_VERSION
export INSTALL_DIR

#export GMSH_LOAD_MODULES=$(echo -e "module load hdf5/1.10.5_${GCC_SHORT}_${MPI_SHORT}\nmodule load med/4.0.0_${GCC_SHORT}_${MPI_SHORT}")

envtpl  --keep-template -o $MODULE_PATH module.tmpl

}

if [[ $1 == "module" ]]
then
  install_module
elif [[ $1 == "clean" ]]
then
  clean_all
else
  install_lib
  install_module
fi

