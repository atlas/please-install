module purge
module load openmpi/1.10.7_gcc640
module load hdf5/1.10.1_gcc640_openmpi1107

pip3 install --user mpi4py
export CC=mpicc
export HDF5_MPI="ON"
export HDF5_DIR=$HDF5_ROOT
pip3 install --user --no-binary=h5py h5py
