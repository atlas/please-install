#!/bin/bash

source ../common.sh

LIB_NAME="gmsh"
LIB_VERSION=4.9.3 #4.4.1 #4.3.0
GCC_VERSION=8.3.0 #6.4.0
MPI_LIB=openmpi
MPI_VERSION=4.0.2 # 1.10.7 #2.1.1 #1.10.7
MPI_SHORT="${MPI_LIB}${MPI_VERSION//.}"
MPI_FULL=${MPI_LIB}-${MPI_VERSION}

LIB_FULLNAME=${LIB_NAME}-${LIB_VERSION}
GCC_FULL=gcc-$GCC_VERSION
GCC_SHORT="gcc${GCC_VERSION//.}"
SUB_DIR=${LIB_NAME}/${LIB_VERSION}/${GCC_FULL}/${MPI_FULL}
WORK_DIR=${BASE_WORK_DIR}/${SUB_DIR}
SRC_DIR=${WORK_DIR}/${LIB_FULLNAME}-source
ARCHIVE=${WORK_DIR}/${LIB_FULLNAME}.tgz
URL="http://gmsh.info/src/gmsh-${LIB_VERSION}-source.tgz"
BUILD_DIR=${WORK_DIR}/${LIB_FULLNAME}-build
INSTALL_DIR=${BASE_INSTALL_DIR}/${SUB_DIR}
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
MODULE_DIR=${BASE_MODULE_DIR}/libs/${LIB_NAME}
MODULE_PATH=${MODULE_DIR}/${LIB_VERSION}_${GCC_SHORT}_${MPI_SHORT}

install_lib()
{
module purge
module load gcc/${GCC_VERSION}
module load ${MPI_LIB}/${MPI_VERSION}_${GCC_SHORT}
module load hdf5/1.10.5_${GCC_SHORT}_${MPI_SHORT}
module load med/4.0.0_${GCC_SHORT}_${MPI_SHORT}
module load intel-mkl/2019.5.281
module load cmake/3.14.0_gcc640
module list

gcc --version |head -n 1
sleep 2

if [[ ! -f $ARCHIVE ]]; then
  mkdir -p $WORK_DIR
  wget $URL -O $ARCHIVE
fi

if [[ ! -d $SRC_DIR ]]; then
  tar zxf $ARCHIVE --directory $WORK_DIR
  # patch for detect mkl
  patch -i $SCRIPT_DIR/mkl.patch $SRC_DIR/CMakeLists.txt || exit 1
fi

if [[ ! -d $BUILD_DIR ]]; then
  mkdir $BUILD_DIR
  cd $BUILD_DIR

  cmake -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} \
  	-DENABLE_MPI=ON \
  	-DENABLE_BUILD_LIB=ON \
  	-DENABLE_BUILD_SHARED=ON \
  	-DCMAKE_BUILD_TYPE=release \
  	-DENABLE_PETSC=OFF \
  	-DCMAKE_CXX_FLAGS="-isystem ${MED_DIR}/include" \
  	-DENABLE_MED=ON -DMED_LIB=${MED_DIR}/lib/libmedC.so \
	-DENABLE_BUILD_DYNAMIC=ON \
  	${SRC_DIR}  || exit 1
fi

# Patch gmsh add this line in gmsh-4.3.0-source/Common/CommandLine.cpp line 27 after inclyde FL.h  and also in gmsh-4.3.0-source/Fltk/helpWindow.cpp
#undef Status 


cd $BUILD_DIR
make -j || exit 1
make -j install || exit 1
}

install_module()
{
cd $SCRIPT_DIR
mkdir -p ${MODULE_DIR}

export LIB_NAME
export LIB_VERSION
export INSTALL_DIR

export GMSH_LOAD_MODULES=$(echo -e "module load hdf5/1.10.5_${GCC_SHORT}_${MPI_SHORT}\nmodule load med/4.0.0_${GCC_SHORT}_${MPI_SHORT}")

envtpl  --keep-template -o $MODULE_PATH module.tmpl
}

if [[ $1 == "module" ]]
then
  install_module
elif [[ $1 == "clean" ]]
then
  clean_all
else
  install_lib
  install_module
fi

